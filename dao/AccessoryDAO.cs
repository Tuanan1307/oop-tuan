﻿using OOP.entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP.dao
{
    public class AccessoryDAO : BaseDAO<Accessory>
    {
        // name of table
        private string TableName { get { return TableName; } set { TableName = "accessory"; } }

        /// <summary>
        /// Get all Accessory in list Accessory by name
        /// </summary>
        /// <param name="name"></param>
        /// <returns>List of Accessory</returns>
        public List<Accessory> Search(string name)
        {
            List<Accessory> accessories = new List<Accessory>();
            foreach (Accessory accessory in database.AccessoryTable)
            {
                if (accessory.Id == Int16.Parse(name) || accessory.Name == name)
                {
                    accessories.Add(accessory);
                }
            }
            return accessories;
        }
    }
}
