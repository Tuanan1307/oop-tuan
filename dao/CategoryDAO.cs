﻿using OOP.entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP.dao
{
    public class CategoryDAO : BaseDAO<Category>
    {
        // name of table
        private string TableName { get { return TableName; } set { TableName = "category"; } }
    }
}
