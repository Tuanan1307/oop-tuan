﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP.entity
{
    // Information of Category
    public class Category : BaseRow
    {
        // Contructor no parameter
        public Category()
        {
        }

        /// <summary>
        /// Contructor parameter
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        public Category(int id, string name) : base(id, name)
        {
        }
    }

}
