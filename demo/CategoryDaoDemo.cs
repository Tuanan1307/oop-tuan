﻿using OOP.dao;
using OOP.entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP.demo
{
    public class CategoryDaoDemo : CategoryDAO
    {
        Category category = new Category();

        // Insert Category in list Category
        public void InsertTest()
        {

            category.Id = database.CategoryTable.Count + 1;
            Console.WriteLine("name of category: ");
            category.Name = Console.ReadLine();
            int result = insert(category);
            if (result == 1)
            {
                Console.WriteLine("insert category success!");
            }
            else
            {
                Console.WriteLine("insert category error!");
            }


        }

        // Get all Category of list Category
        public void FindAllTest()
        {
            List<object> objects = new List<object>();
            objects = findAll();
            foreach (Category category in objects)
            {
                Console.WriteLine(category.Id + " " + category.Name);
            }
        }

        // Update Category in list Category
        public void UpdateTest()
        {
            Console.WriteLine("id of category: ");
            category.Id = Int16.Parse(Console.ReadLine());
            Console.WriteLine("name of category");
            category.Name = Console.ReadLine();
            int result = update(category);
            if (result == 1)
            {
                Console.WriteLine("Update Category success!");
            }
            else
            {
                Console.WriteLine("Update Category error!");
            }
        }
    }
}
