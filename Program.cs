﻿using OOP.dao;
using OOP.demo;
using OOP.entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP
{
    public class Program
    {
        public static void Main(string[] args)
        {
            List<Product> products = new List<Product>()
              {
                new Product(1,"Tuan",  123 ) ,
                new Product(2,"Theo", 123)   ,
                new Product(3,"Phuong", 123)
              };

            Product product = new Product();
            ProductDemo productDemo = new ProductDemo();
            //product = productDemo.CreateProductTest(4, "Dung", 123);
            //Console.WriteLine(product.Id);
            //Console.WriteLine("dwadaw");
            productDemo.PrintProduct(products);
            //Console.WriteLine();
            //Console.WriteLine();
            //DatabaseDemo databaseDemo = new DatabaseDemo();
            //databaseDemo.InsertTableTest();
            //Console.ReadLine();
            /*  foreach (Product Products in products)
              {
                  Console.WriteLine(Products.Id + "" + Products.Name);
              }
                  Console.WriteLine(product.Id + product.Name + product.CategoryId);

                   Console.ReadLine();
              }*/

            DatabaseDemo dbDemo = new DatabaseDemo();
            Database database = new Database();
            Console.WriteLine("insert");
            database.InsertTable($"{database.ProductTable}", product);
            dbDemo.InsertTableTest();
            Console.ReadLine();
        }
    }
}
